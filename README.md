# Truffle Health - Software Engineering Assesment

## Displaying Sample Data
<img src="screenshots/SampleData.png" alt="">

## Displaying Test Cases
<img src="screenshots/TestCase.png" alt="">

# Installation

To install the dependencies, run the following command in the terminal:

```bash
npm install
```

# Usage

To start the server, run the following command in the terminal:

```bash
npm start
```

This will start the server on port 3000.

# Endpoints
'GET /items'
Returns a list of medical bills.

'POST /items'
Creates a new medical bills.

# Testing
To run the test suite using Jest, run the following command in the terminal:

```bash
npm test
```
This will run the 'server.test.js' file and output the test results to the console.

That's it! You should now be able to use and test the medical bill API implementation.

# Postman API Running Screenshots
<img src="screenshots/Postman1.png" alt="">
<img src="screenshots/Postman2.png" alt="">
