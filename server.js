const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());

// Sample data
let medicalBills = [
  {
    id: 1,
    patient: {
      name: 'Naman Mehta',
      address: 'Ghatkopar, Mumbai, India'
    },
    hospital: 'Fortis',
    dateOfService: '2022-01-15',
    billAmount: 10000
  },
  {
    id: 2,
    patient: {
      name: 'Deepam Dholakia',
      address: 'Dadar, Mumbai, India'
    },
    hospital: 'Xynova',
    dateOfService: '2022-02-01',
    billAmount: 50000
  }
];
//GET
app.get('/items', (req, res) => {
  res.json(medicalBills);
});
//POST
app.post('/items', (req, res) => {
  const newBill = req.body;
  newBill.id = medicalBills.length + 1; //incrementing ID
  medicalBills.push(newBill);
  res.status(201).json(newBill);
});
//LISTEN
app.listen(3000, () => {
  console.log('Server started on port 3000');
});
