const http = require('http');
const server = require('./server');

//GET
describe('GET /items', () => {
  it('It should return a list of medical bills', (done) => {
    const options = {
      hostname: 'localhost',
      port: 3000,
      path: '/items',
      method: 'GET'
    };

    const req = http.request(options, (res) => {
      expect(res.statusCode).toBe(200);
      let data = '';
      res.on('data', (chunk) => {
        data += chunk;
      });
      res.on('end', () => {
        const body = JSON.parse(data);
        expect(body).toEqual(expect.any(Array));
        done();
      });
    });

    req.on('error', (err) => {
      done(err);
    });

    req.end();
  });
});
/*
{
  "patient": {
      "name": "Smit Parekh",
      "address": "India"
  },
  "hospital": "Ashirvaad Hospital",
  "dateOfService": "2022-02-15",
  "billAmount": 75000
}
*/
//POST
describe('POST /items', () => {
  it('It should create a new medical bill', (done) => {
    const newBill = {
      patient: {
        name: 'Smit Parekh',
        address: 'India'
      },
      hospital: 'Ashirvaad Hospital',
      dateOfService: '2022-02-15',
      billAmount: 75000
    };

    const options = {
      hostname: 'localhost',
      port: 3000,
      path: '/items',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      }
    };

    const req = http.request(options, (res) => {
      expect(res.statusCode).toBe(201);
      let data = '';
      res.on('data', (chunk) => {
        data += chunk;
      });
      res.on('end', () => {
        const body = JSON.parse(data);
        expect(body).toMatchObject(newBill);
        done();
      });
    });

    req.on('error', (err) => {
      done(err);
    });

    req.write(JSON.stringify(newBill));
    req.end();
  });
});
